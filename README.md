# Universis API Metrics Plugin
This is a plugin for the universis-api that will be returning the metrics of the node service
running on the server.

## Getting Started 
These instructions will get you a copy of the project up and running on your local machine 
for development and testing purposes.
### Prerequisites
    1. Git
    2. Nodejs
### Instructions

First clone the repository: 

`git clone https://gitlab.com/universis/universis-metrics.git `

Go to the repository's directory: 

`cd universis-metrics`

Install dependencies:

`npm install`

#### For Development

Build the package

`npm run build:dev`

Link the dist of package

`cd dist && npm link`

In the universis-api run 

`npm link universis-metrics`

#### For production 

Build the package

`npm run build`

Link the dist of package

`npm pack ./dist/`

In the universis-api run 

`npm i ../universis-metrics/universis-metrics-*.tgz`

#### Both development and production installations

Add the necessary services in the app.development.json

```
...
{
    "serviceType": "universis-metrics/modules/metrics#HeapDetailsPlugin"
}
...
``` 

Start a new api process 

`npm run start`

Finally start you api server
and use `http://localhost:5001/api/metrics/*` endpoints to view the metrics of the api
