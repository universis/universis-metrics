const Jasmine = require('jasmine');
const SpecReporter = require('jasmine-spec-reporter').SpecReporter;

const jasmine = new Jasmine();
jasmine.getEnv().clearReporters();               // remove default reporter logs
jasmine.getEnv().addReporter(new SpecReporter({  // add jasmine-spec-reporter
	spec: {
		displayPending: true,
		displayStacktrace: true
	}
}));
jasmine.DEFAULT_TIMEOUT_INTERVAL = 30000;
