const METRICS_ENUM = [
	{name: 'up', help: 'Is the process running'},
	{name: 'cpu', help: 'Process cpu usage'},
	{name: 'memory', help: 'Process memory usage'},
	{name: 'heapSize', help: 'Process heap size'},
	{name: 'usedHeapSize', help: 'Process heap usage'},
	{name: 'uptime', help: 'Process uptime'},
	{name: 'instances', help: 'Process instances'},
	{name: 'restarts', help: 'Process restarts'},
	{name: 'loopDelay', help: 'Event Loop Latency'},
	{name: 'loopDelayP95', help: 'Event Loop Latency p95'}
];

export {METRICS_ENUM};
