import {ApplicationService, Args} from '@themost/common';
import pm2 from 'pm2';
import {METRICS_ENUM} from './metrics';
import cluster from 'node:cluster';
import process from 'node:process';
import v8 from 'node:v8';
import fs from 'node:fs';
import perf_hooks from 'node:perf_hooks';
import {PrometheusExporter} from '@opentelemetry/exporter-prometheus';
import {JaegerExporter} from '@opentelemetry/exporter-jaeger';
import {ZipkinExporter} from '@opentelemetry/exporter-zipkin';
import {NodeSDK} from '@opentelemetry/sdk-node';
import {getNodeAutoInstrumentations} from '@opentelemetry/auto-instrumentations-node';
// eslint-disable-next-line no-unused-vars
import {Meter, MeterProvider} from '@opentelemetry/sdk-metrics-base';
import {HttpInstrumentation} from '@opentelemetry/instrumentation-http';
import {ExpressInstrumentation} from '@opentelemetry/instrumentation-express';
import {TediousInstrumentation} from '@opentelemetry/instrumentation-tedious';
import {FsInstrumentation} from '@opentelemetry/instrumentation-fs';
import {HostMetrics} from '@opentelemetry/host-metrics';
import {ValueType} from '@opentelemetry/api-metrics';
import {TraceUtils} from '@themost/common';
import {Resource} from '@opentelemetry/resources';
import {SemanticResourceAttributes} from '@opentelemetry/semantic-conventions';
const client = require('prom-client');
const gc_stats = require('gc-stats');


class TelemetryService extends ApplicationService {

	constructor(app) {
		super(app);
		let telemetryConfiguration;
		try {
			telemetryConfiguration = app.getConfiguration().getSourceAt('settings/universis/telemetry');
			this._checkConfiguration(telemetryConfiguration);
		} catch (err) {
			throw new Error(`Error while loading Telemetry Service settings. ${err}`);
		}
		let metricsConfiguration = telemetryConfiguration.metrics;
		let tracingConfiguration = telemetryConfiguration.tracing;

		const meterProvider = new MeterProvider();
		const prometheus = new PrometheusExporter({
			host: metricsConfiguration.host,
			port: metricsConfiguration.port ?? 8081,
			endpoint: metricsConfiguration.endpoint ?? 'metrics'
		});
		let tracer;
		if (tracingConfiguration.exporter.toLowerCase().startsWith('z')) {
			let url;
			if (tracingConfiguration?.useSecureConnection) {
				url = `https://${tracingConfiguration.host}:${tracingConfiguration.port ?? 9411}/api/v2/spans`;
			} else {
				url = `http://${tracingConfiguration.host}:${tracingConfiguration.port ?? 9411}/api/v2/spans`;
			}
			tracer = new ZipkinExporter({
				url: url,
				serviceName: telemetryConfiguration.serviceName,
			});
		} else {
			tracer = new JaegerExporter({
				host: tracingConfiguration.host,
				port: tracingConfiguration.port ?? 9411,
				username: tracingConfiguration?.username ?? undefined,
				password: tracingConfiguration?.password ?? undefined,
			});
		}
		const sdk = new NodeSDK({
			resource: new Resource({
				[SemanticResourceAttributes.SERVICE_NAME]: telemetryConfiguration.serviceName
			}),
			autoDetectResources: true,
			traceExporter: tracer,
			instrumentations: [
				(new HttpInstrumentation({enabled: true, serverName: 'universis-api-private'})),
				(new ExpressInstrumentation({enabled: true})),
				(new TediousInstrumentation({enabled: true})),
				(new FsInstrumentation({enabled: true})),
				getNodeAutoInstrumentations()
			]
		});
		sdk.start().then(() => {
		});
		meterProvider.addMetricReader(prometheus);
		let meter = meterProvider.getMeter('prometheus');

		this._configureHttpMetrics(meter);

		this._configureFileDescriptorMetrics(meter);

		this._configureGarbageCollectorMetrics(meter);

		this._configureMemoryMetrics(meter);

		this._configureContextMetrics(meter);

		this._configureHostMetrics(meterProvider);
		process.on('SIGTERM', () => {
			sdk
				.shutdown()
				.then(
					() => console.log('SDK shut down successfully'),
					(err) => console.log('Error shutting down SDK', err)
				)
				.finally(() => process.exit(0));
		});
	}


	_checkConfiguration(configuration) {
		Args.notNull(configuration?.serviceName, 'telemetryConfiguration.serviceName');
		Args.notNull(configuration?.metrics, 'telemetryConfiguration.metrics');
		Args.notNull(configuration?.metrics?.exporter, 'telemetryConfiguration.metrics.exporter');
		Args.notNull(configuration?.metrics?.host, 'telemetryConfiguration.metrics.host');
		Args.notNull(configuration?.metrics?.port, 'telemetryConfiguration.metrics.port');
		Args.notNumber(configuration?.metrics?.port, 'telemetryConfiguration.metrics.port');
		Args.notNull(configuration?.metrics?.endpoint, 'telemetryConfiguration.metrics.endpoint');
		Args.notString(configuration?.metrics?.endpoint, 'telemetryConfiguration.metrics.endpoint');
		Args.notNull(configuration?.tracing, 'telemetryConfiguration.tracing');
		Args.notNull(configuration?.tracing?.exporter, 'telemetryConfiguration.tracing.exporter');
		Args.notNull(configuration?.tracing?.host, 'telemetryConfiguration.tracing.host');
		Args.notNull(configuration?.tracing?.port, 'telemetryConfiguration.tracing.port');
		Args.notNumber(configuration?.tracing?.port, 'telemetryConfiguration.tracing.port');
	}

	/**
	 *
	 * @param meter {MeterProvider}
	 * @private
	 */
	_configureHostMetrics(meter){
		const hostMetrics = new HostMetrics({meterProvider: meter, name: 'universis-api-private'});
		hostMetrics.start();
	}

	/**
	 *
	 * @param meter {Meter}
	 * @private
	 */
	_configureHttpMetrics(meter) {
		let httpHistogram = meter.createHistogram('api_nodejs_http', {
			description: 'http details for get and post',
			valueType: ValueType.DOUBLE
		});
		httpHistogram.record(1000, {'http.method': 'GET', 'http.scheme': 'http'});
		httpHistogram.record(1000, {'http.method': 'GET', 'http.scheme': 'https', 'http.target': '/api'});
		httpHistogram.record(1000, {'http.method': 'POST', 'http.scheme': 'http'});
		httpHistogram.record(1000, {'http.method': 'POST', 'http.scheme': 'https', 'http.target': '/api'});

	}

	/**
	 *
	 * @param meter {Meter}
	 * @private
	 */
	_configureFileDescriptorMetrics(meter){
		let fdMetrics = meter.createObservableGauge('api_nodejs_open_fds_count', {
			description: 'number of open file descriptors',
			valueType: ValueType.INT
		});
		fdMetrics.addCallback((observableResult) => {
			try {
				const fds = fs.readdirSync('/proc/self/fd');
				observableResult.observe(fds.length -1);
			} catch (e) {
				// noop
			}
		});

		let maxFds;
		let maxFdsMetrics = meter.createObservableGauge('api_nodejs_max_fds_count');
		maxFdsMetrics.addCallback((observableResult) => {
			try {
				const limits = fs.readFileSync('/proc/self/limits', 'utf8');
				const lines = limits.split('\n');
				for (const line of lines) {
					if (line.startsWith('Max open files')) {
						const parts = line.split(/  +/);
						maxFds = Number(parts[1]);
						break;
					}
				}
			} catch (e) {
				// no-op
			}
			observableResult.observe(maxFds);
		});
	}

	/**
	 *
	 * @param meter {Meter}
	 * @private
	 */
	_configureGarbageCollectorMetrics(meter) {
		let gc_details = meter.createHistogram('nodejs_gc_duration_seconds', {
			description: 'garbage collection details'
		});

		const scavengeEvents = meter.createHistogram('api_nodejs_gc_young_events_count', {
			'description': 'scavenge/young events'
		});
		const markSweepCompactEvents = meter.createHistogram('api_nodejs_gc_old_events_count', {
			'description': 'old events'
		});
		const incrementalEvents = meter.createHistogram('api_nodejs_gc_incremental_events_count', {
			'description': 'incremental events'
		});
		const weakCallbackEvents = meter.createHistogram('api_nodejs_gc_weak_callback_events_count', {
			'description': 'weak callback events'
		});
		const obs = new perf_hooks.PerformanceObserver(list => {
			const entry = list.getEntries()[0];
			gc_details.record(entry.duration / 1000);
			if (entry.entryType === 'gc') {
				switch (entry.kind){
				case (perf_hooks.constants.NODE_PERFORMANCE_GC_MINOR): {
					scavengeEvents.record(1);
					break;
				}
				case (perf_hooks.constants.NODE_PERFORMANCE_GC_MAJOR): {
					markSweepCompactEvents.record(1);
					break;
				}
				case (perf_hooks.constants.NODE_PERFORMANCE_GC_INCREMENTAL): {
					incrementalEvents.record(1);
					break;
				}
				case (perf_hooks.constants.NODE_PERFORMANCE_GC_WEAKCB): {
					weakCallbackEvents.record(1);
					break;
				}
				default: {
					//no-op
					break;
				}
				}
			}
		});

		obs.observe({ entryTypes: ['gc'] });


		let reclaimed_bytes = meter.createCounter('api_nodejs_gc_reclaimed_bytes_total', {
			valueType: ValueType.INT,
			description: 'number of garbage collection runs'
		});
		let gcTimeCount = meter.createCounter('api_nodejs_gc_pause_seconds_total', {
			valueType: ValueType.INT
		});
		gc_stats().on('stats', stats => {
			gcTimeCount.add(stats.pause / 1e9);
			if (stats.diff.usedHeapSize < 0) {
				reclaimed_bytes.add(stats.diff.usedHeapSize* -1);
			}
		});

	}


	/**
	 *
	 * @param meter {Meter}
	 * @private
	 */
	_configureContextMetrics(meter){
		let nativeContexts = undefined;
		let nativeContextsMetric = meter.createObservableGauge('api_nodejs_total_number_of_native_contexts');
		nativeContextsMetric.addCallback((observableResult) => {
			try {
				nativeContexts = v8.getHeapStatistics();
				observableResult.observe(nativeContexts.number_of_native_contexts);
			} catch (e) {
				// no-op
			}
		});
		let detachedContexts = undefined;
		let detachedContextsMetric = meter.createObservableGauge('api_nodejs_total_number_of_detached_contexts');
		detachedContextsMetric.addCallback((observableResult) => {
			try {
				detachedContexts = v8.getHeapStatistics();
				observableResult.observe(detachedContexts.number_of_detached_contexts);
			} catch (e) {
				// no-op
			}
		});
	}

	/**
	 *
	 * @param meter {Meter}
	 * @private
	 */
	_configureMemoryMetrics(meter){
		let rss_memory = meter.createObservableGauge('api_nodejs_rss_memory_in_bytes');
		rss_memory.addCallback((observableResult) => {
			let rss = undefined;
			try {
				rss = process.memoryUsage().rss;
				observableResult.observe(rss);
			} catch (e) {
				// no-op
			}
		});

		let external_memory = meter.createObservableGauge('api_nodejs_external_memory_in_bytes');
		external_memory.addCallback((observableResult) => {
			let ext = undefined;
			try {
				ext = process.memoryUsage().external;
				observableResult.observe(ext);
			} catch (e) {
				// no-op
			}
		});
	}

	/**
	 * Use of pm2 CLI tool
	 */
	pm2exec(cmd, ...args) {
		return new Promise((resolve, reject) => {
			pm2[cmd](...args, (err, resp) => (err ? reject(err) : resolve(resp)));
		});
	}

	/**
	 * Filters out active from inactive/launching/errored instances
	 */
	getOnlineInstances(instancesData) {
		return instancesData.filter(({pm2_env}) => pm2_env.status === 'online');
	}

	/**
	 * Here we are getting the main stats that are recommended by prometheus.
	 * @params instancesData : an array of pm2 instances running
	 */
	getMainMetricsRegister(instancesData) {
		// don't use prom.register here because these metrics
		// will be summed in cluster mode!
		const registry = new client.Registry();
		const mainMetrics = METRICS_ENUM.reduce((acc, {name, help}) => {
			acc[name] = new client.Gauge({
				name,
				help,
				labelNames: ['name', 'instance'],
				registers: [registry],
			});

			return acc;
		}, {});

		instancesData.forEach(({name, pm2_env, monit}) => {
			const conf = {
				name: name,
				instance: pm2_env.pm_id,
			};
			const axm = pm2_env.axm_monitor;
			const values = {
				up: pm2_env.status === 'online' ? 1 : 0,
				cpu: monit.cpu,
				memory: monit.memory,
				heapSize: parseFloat(axm['Heap Size'].value) || null,
				usedHeapSize: parseFloat(axm['Used Heap Size'].value) || null,
				uptime: Math.round((Date.now() - pm2_env.pm_uptime) / 1000),
				instances: pm2_env.instances || 1,
				restarts: pm2_env.unstable_restarts + pm2_env.restart_time,
				loopDelay: parseFloat(axm['Event Loop Latency'].value) || null,
				loopDelayP95: parseFloat(axm['Event Loop Latency p95'].value) || null,
			};
			TraceUtils.debug('Got through values', values);
			Object.entries(values).forEach(([name, value]) => {
				if (value !== null) {
					mainMetrics[name].set(conf, value);
				}
			});
		});

		return registry;
	}

	/**
	 * This is called from the process that gets to process the incoming request
	 * In node.js processes can communicate with each other by passing message to
	 * each other and can also get callbacks. Here we are passing a message to the
	 * processes in `instancesData` for them to also collect the heap details.
	 * If an error occurs we are stopping the execution for that instance
	 * and logging the error.
	 */
	requestNeighboursData(instancesData, instancesToWait) {
		const targetInstanceId = Number(process.env.pm_id);
		const data = {topic: this.REQ_TOPIC, data: {targetInstanceId}};

		Object.values(instancesData).forEach(({pm_id}) => {
			if (pm_id !== targetInstanceId) {
				this.pm2exec('sendDataToProcessId', pm_id, data).catch(e => {
					instancesToWait.count--;

					TraceUtils.error(`Failed to request metrics from instance #${pm_id}: ${e.message}`);
				});
			}
		});
	}

	getCurrentRegistry(instancesData) {
		return client.AggregatorRegistry.aggregate([
			this.getMainMetricsRegister(instancesData).getMetricsAsJSON(),
			client.register.getMetricsAsJSON(),
		]);
	}

	/**
	 * Gets aggregated metrics for the workers.
	 * @param instancesData : array of workers
	 */
	async getAggregatedRegistry(instancesData) {
		const onlineInstances = this.getOnlineInstances(instancesData);
		let instancesToWait = {count: onlineInstances.length};

		// eslint-disable-next-line no-async-promise-executor
		const registryPromise = new Promise(async (resolve, reject) => {
			const registersList = [];
			const instanceId = Number(process.env.pm_id);
			TraceUtils.debug('Got through instanceID', instanceId);
			const eventName = `process:${instanceId}`;
			let responseCount = 1;
			let timeoutId;
			const self = this;

			function sendResult() {
				self.pm2Bus.off(eventName);
				TraceUtils.debug('Got through instanceID', self.pm2Bus);
				resolve(client.AggregatorRegistry.aggregate(registersList));
			}

			function kickNoResponseTimeout() {
				timeoutId = setTimeout(() => {
					TraceUtils.warn(
						`Metrics were sent by timeout. No response from ${instancesToWait.count -
						responseCount} instances.`,
					);
					sendResult();
				}, 1000);
			}

			try {
				registersList[instanceId] = this.getCurrentRegistry(
					instancesData,
				).getMetricsAsJSON();

				if (!this.pm2Bus) {
					this.pm2Bus = await this.pm2exec('launchBus');
				}

				kickNoResponseTimeout();

				this.pm2Bus.on(eventName, packet => {
					registersList[packet.data.instanceId] = packet.data.register;
					responseCount++;
					clearTimeout(timeoutId);

					if (responseCount === instancesToWait.count) {
						sendResult();
					} else {
						kickNoResponseTimeout();
					}
				});
			} catch (e) {
				reject(e);
			}
		});
		// this function must be called after the registryPromise declaration
		// because requests have to be sent after the listener was set up.
		this.requestNeighboursData(onlineInstances, instancesToWait);

		return registryPromise;
	}


	async getHeapDetails() {
		let result = [];
		/* this will be executed when the API server is running
		 * in cluster mode. It will send a message to the rest of the
		 * node workers to get their heap statistics
		 */
		if (!cluster.isMaster) {
			process.on('message', packet => {
				if (packet.topic === this.REQ_TOPIC) {
					process.send({
						type: `process:${packet.data.targetInstanceId}`,
						data: {
							instanceId: Number(process.env.pm_id),
							register: client.register.getMetricsAsJSON(),
						},
					});
				}
			});

			/*
			 * Uses pm2 CLI
			 */
			await this.pm2exec('connect');

			try {
				/*
					* gets the list of all the node instances running under pm2
					* either the master process or the workers
				 */
				const instancesData = await this.pm2exec('list');
				const register =
					this.getOnlineInstances(instancesData).length > 1
						? await this.getAggregatedRegistry(instancesData)
						: await this.getCurrentRegistry(instancesData);

				const aggregatedMetrics = register.getMetricsAsJSON();
				/*
					* Also gets the heap stats of the process that
					* was used to process the request, mostly for compatibility
					* purposes with the single instance heap stats. This way,
					* it will be easier to visualize the data obtained by this API call
				 */
				const heap = process.memoryUsage();
				const maxRss = process.resourceUsage().maxRSS;
				result.push({...heap, maxRSS: maxRss, metrics: aggregatedMetrics});
			} catch (err) {
				TraceUtils.error(`Failed to get metrics: ${err.message}`);
			}
		} else {
			/*
				* Runs in single node instances
			 */
			const heap = process.memoryUsage();
			const maxRss = process.resourceUsage().maxRSS;
			/*
				* Also pushes metrics field to keep the compatibility
				* with cluster mode api instances
			 */
			result.push({...heap, maxRSS: maxRss, metrics: []});
		}
		return result;
	}
}

module.exports = {
	TelemetryService
};
